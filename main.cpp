#include <iostream>
#include <fstream>
#include <vector>
#include "Person.h"
using namespace std;

int main()
{
    ifstream file("Osoby.txt");
    
    vector<Person> persons;
    
    Person temp;
    while (file >> temp)
    {
        persons.push_back(temp);
    }
    
    Person sum;
    for (Person p : persons)
    {
        sum += p;
    }
    cout << sum.getAge();
    
    file.close();
    return 0;
}
