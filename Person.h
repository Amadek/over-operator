#ifndef PERSON_H
#define PERSON_H
#include <iostream>
#include <string>
using std::string;

class Person
{
private:
    string name;
    string surname;
    int age;
public:
    Person();
    Person(string, string, int);
    
    string getName();
    string getSurname();
    int getAge();
    
    void setName(string);
    void setSurname(string);
    void setAge(int);
    
    Person operator+(const Person&);
    
    friend std::ostream & operator<<(std::ostream&, const Person&);
    friend std::istream & operator>>(std::istream&, Person&);
    
    static Person getRandom();
};

#endif /* PERSON_H */

