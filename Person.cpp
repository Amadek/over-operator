#include "Person.h"

Person::Person() : name(""), surname(""), age(0) { }

Person::Person(
    string name,
    string surname,
    int age
) : name(name), surname(surname), age(age) { }

/*********************************************
 * GET
 */

string Person::getName()
{
    return name;
}

string Person::getSurname()
{
    return surname;
}

int Person::getAge()
{
    return age;
}

std::ostream & operator<<(std::ostream & os, const Person & p)
{
    os << p.name << " " << p.surname << " " << p.age << std::endl;
    return os;
}

/*********************************************
 * SET
 */

void Person::setName(string name)
{
    this->name = name;
}

void Person::setSurname(string surname)
{
    this->surname = surname;
}

void Person::setAge(int age)
{
    this->age = age;
}

std::istream & operator>>(std::istream & is, Person & p)
{
    is >> p.name >> p.surname >> p.age;
    return is;
}

/*********************************************
 * OPERATIONS
 */

Person Person::operator+(const Person & p)
{
    Person sum;
    sum.age = this->age + p.age;
    return sum;
}

/*********************************************
 * STATIC
 */

Person Person::getRandom()
{
    char alfa[27] = "ABCDEFGHIJKLMNOPQRSTVWUXYZ";
    
    int age = rand() % 100 + 1;
    string name = "";
    string surname = "";
    
    for (int i = 0; i < 8; i++)
    {
        if (i < 4)
            name += alfa[rand() % 26];
        
        surname += alfa[rand() % 26];
    }
    
    return Person(name, surname, age);
}